import { GameState } from '../GameState';

describe('GameState', () => {
	test('should have the correct values', () => {
		expect(GameState.CONTINUE).toBe('CONTINUE');
		expect(GameState.X_WINS).toBe('X_WINS');
		expect(GameState.O_WINS).toBe('O_WINS');
		expect(GameState.DRAW).toBe('DRAW');
		expect(GameState.STOPPED).toBe('STOPPED');
	});
});
