import TicTacToe from '../main';

describe('TicTacToe', () => {
	beforeEach(() => {
		document.body.innerHTML = `
            <div id="game-container">
                <div id="board" class="board">
                    <div class="cell" data-row="0" data-col="0"></div>
                    <div class="cell" data-row="0" data-col="1"></div>
                    <div class="cell" data-row="0" data-col="2"></div>
                    <div class="cell" data-row="1" data-col="0"></div>
                    <div class="cell" data-row="1" data-col="1"></div>
                    <div class="cell" data-row="1" data-col="2"></div>
                    <div class="cell" data-row="2" data-col="0"></div>
                    <div class="cell" data-row="2" data-col="1"></div>
                    <div class="cell" data-row="2" data-col="2"></div>
                </div>
                <button id="reset-button">Reset Game</button>
                <div id="status">Player X's turn</div>
            </div>
            <div id="leader-board"></div>
        `;
		new TicTacToe();
	});

	test('should initialize the game correctly', () => {
		expect(document.querySelector('#status')?.textContent).toBe(
			"Player X's turn"
		);
	});

	test('should handle a cell click correctly', () => {
		const cell = document.querySelector(
			'.cell[data-row="0"][data-col="0"]'
		) as HTMLElement;
		cell.click();
		expect(cell.textContent).toBe('X');
		expect(document.querySelector('#status')?.textContent).toBe(
			"Player O's turn"
		);
	});

	test('should reset the game correctly', () => {
		const cell = document.querySelector(
			'.cell[data-row="0"][data-col="0"]'
		) as HTMLElement;
		cell.click();
		const resetButton = document.querySelector('#reset-button') as HTMLElement;
		resetButton.click();
		expect(cell.textContent).toBe('');
		expect(document.querySelector('#status')?.textContent).toBe(
			"Player X's turn"
		);
	});

	test('should declare X as the winner', () => {
		const cells = [
			document.querySelector(
				'.cell[data-row="0"][data-col="0"]'
			) as HTMLElement,
			document.querySelector(
				'.cell[data-row="0"][data-col="1"]'
			) as HTMLElement,
			document.querySelector(
				'.cell[data-row="0"][data-col="2"]'
			) as HTMLElement,
		];
		cells.forEach((cell) => cell.click());
		expect(document.querySelector('#status')?.textContent).toBe(
			'Player X wins!'
		);
	});

	test('should declare a draw', () => {
		const moves = [
			[0, 0],
			[0, 1],
			[0, 2],
			[1, 0],
			[1, 1],
			[1, 2],
			[2, 0],
			[2, 1],
			[2, 2],
		];

		const symbols = ['X', 'O', 'X', 'X', 'X', 'O', 'O', 'X', 'O'];

		moves.forEach(([row, col], index) => {
			const cell = document.querySelector(
				`.cell[data-row="${row}"][data-col="${col}"]`
			) as HTMLElement;
			cell.click();
			expect(cell.textContent).toBe(symbols[index]);
		});

		expect(document.querySelector('#status')?.textContent).toBe("It's a draw!");
	});

	test('should not allow a move after the game is won', () => {
		const winningMoves = [
			[0, 0],
			[1, 0],
			[0, 1],
			[1, 1],
			[0, 2],
		];

		winningMoves.forEach(([row, col]) => {
			const cell = document.querySelector(
				`.cell[data-row="${row}"][data-col="${col}"]`
			) as HTMLElement;
			cell.click();
		});

		const status = document.querySelector('#status')?.textContent;
		expect(status).toBe('Player X wins!');

		// Attempt to make another move after the game is won
		const extraMoveCell = document.querySelector(
			'.cell[data-row="2"][data-col="2"]'
		) as HTMLElement;
		extraMoveCell.click();
		expect(extraMoveCell.textContent).toBe('');
	});
});
