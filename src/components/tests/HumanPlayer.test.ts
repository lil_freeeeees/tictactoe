import { HumanPlayer } from '../HumanPlayer';

describe('HumanPlayer', () => {
	let player: HumanPlayer;

	beforeEach(() => {
		document.body.innerHTML =
			'<div class="cell" data-row="0" data-col="0"></div>';
		player = new HumanPlayer('Player X', 'X');
	});

	test('should create a player with the correct name and symbol', () => {
		expect(player.name).toBe('Player X');
		expect(player.symbol).toBe('X');
	});

	test('should call makeMove method correctly', () => {
		const cell = document.querySelector('.cell') as HTMLElement;
		const makeMoveSpy = jest.spyOn(player, 'makeMove');
		cell.click();
		expect(makeMoveSpy).toHaveBeenCalledWith(0, 0);
	});

	test('makeMove should update the cell content correctly', () => {
		const cell = document.querySelector(
			'.cell[data-row="0"][data-col="0"]'
		) as HTMLElement;
		player.makeMove(0, 0);
		expect(cell.textContent).toBe('X');
	});
});
