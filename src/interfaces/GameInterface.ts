/**
 * Interface representing a player in the game.
 */
interface IPlayer {
	name: string;
	symbol: 'X' | 'O'; // Player's symbol
	makeMove: (board: ICell[][]) => void; // Function to make a move on the board
}

/**
 * Interface representing a cell on the game board.
 */
interface ICell {
	row: number;
	col: number;
	value: 'X' | 'O' | null; // Cell can be empty (null), or filled with 'X' or 'O'
}

/**
 * Interface for the game board.
 */
interface IGameBoard {
	cells: ICell[][]; // Two-dimensional array of cells
	checkWin: () => boolean; // Function to check if there's a winning condition
	checkDraw: () => boolean; // Function to check if the game is a draw
}

/**
 * Interface for the game state management.
 */
interface IGameState {
	currentPlayer: IPlayer;
	board: IGameBoard;
	gameState: 'CONTINUE' | 'X_WINS' | 'O_WINS' | 'DRAW' | 'STOPPED';
	changePlayer: () => void; // Function to switch the current player
	resetGame: () => void; // Function to reset the game to its initial state
}

export type { IPlayer, ICell, IGameBoard, IGameState };
