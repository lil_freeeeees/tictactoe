/**
 * Abstract class representing a player in the Tic Tac Toe game.
 */
abstract class Player {
	name: string;
	symbol: 'X' | 'O';

	/**
	 * Create a player.
	 * @param name The name of the player.
	 * @param symbol The symbol the player uses ('X' or 'O').
	 */
	constructor(name: string, symbol: 'X' | 'O') {
		this.name = name;
		this.symbol = symbol;
	}

	/**
	 * Abstract method to make a move on the game board.
	 * This method should be implemented by subclasses to define
	 * how the player makes a move.
	 * @param board The current state of the game board.
	 * @returns The coordinates (row and column) where the move was made.
	 */
	abstract makeMove(row: number, col: number): void;
}

export { Player };
