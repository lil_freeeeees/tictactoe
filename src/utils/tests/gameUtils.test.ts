import { initializeBoard, checkWin, checkDraw } from '../gameUtils';

describe('gameUtils', () => {
	test('initializeBoard should create a 3x3 board', () => {
		const board = initializeBoard();
		expect(board.length).toBe(3);
		board.forEach((row) => expect(row.length).toBe(3));
	});

	test('checkWin should detect a winning condition', () => {
		const board = initializeBoard();
		board[0][0].value = 'X';
		board[0][1].value = 'X';
		board[0][2].value = 'X';
		expect(checkWin(board)).toBe(true);
	});

	test('checkWin should return false for no winning condition', () => {
		const board = initializeBoard();
		board[0][0].value = 'X';
		board[0][1].value = 'O';
		board[0][2].value = 'X';
		expect(checkWin(board)).toBe(false);
	});

	test('checkDraw should detect a draw condition', () => {
		const board = initializeBoard();
		board.forEach((row) => row.forEach((cell) => (cell.value = 'X')));
		expect(checkDraw(board)).toBe(true);
	});

	test('checkDraw should return false for non-draw condition', () => {
		const board = initializeBoard();
		board[0][0].value = 'X';
		expect(checkDraw(board)).toBe(false);
	});
});
