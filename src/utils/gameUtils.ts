import { ICell } from '../interfaces/GameInterface';

/**
 * Initialize the game board with empty cells.
 * @returns A 3x3 matrix of cells, all initially null.
 */
function initializeBoard(): ICell[][] {
	const board: ICell[][] = [];
	for (let row = 0; row < 3; row++) {
		board[row] = [];
		for (let col = 0; col < 3; col++) {
			board[row].push({
				row: row,
				col: col,
				value: null,
			});
		}
	}
	return board;
}

/**
 * Check if there is a winner on the board.
 * @param board The current state of the game board.
 * @returns True if there is a winner, false otherwise.
 */
function checkWin(board: ICell[][]): boolean {
	const lines = [
		// Rows
		[board[0][0], board[0][1], board[0][2]],
		[board[1][0], board[1][1], board[1][2]],
		[board[2][0], board[2][1], board[2][2]],
		// Columns
		[board[0][0], board[1][0], board[2][0]],
		[board[0][1], board[1][1], board[2][1]],
		[board[0][2], board[1][2], board[2][2]],
		// Diagonals
		[board[0][0], board[1][1], board[2][2]],
		[board[0][2], board[1][1], board[2][0]],
	];

	for (let line of lines) {
		if (
			line[0].value &&
			line[0].value === line[1].value &&
			line[0].value === line[2].value
		) {
			return true;
		}
	}

	return false;
}

/**
 * Check if the game is a draw.
 * @param board The current state of the game board.
 * @returns True if the game is a draw, false otherwise.
 */
function checkDraw(board: ICell[][]): boolean {
	return (
		board.every((row) => row.every((cell) => cell.value !== null)) &&
		!checkWin(board)
	);
}

export { initializeBoard, checkWin, checkDraw };
