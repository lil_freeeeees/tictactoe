import { LeaderBoard } from '../LeaderBoard';

describe('LeaderBoard', () => {
	let leaderBoard: LeaderBoard;

	beforeEach(() => {
		document.body.innerHTML = '<div id="leader-board"></div>';
		leaderBoard = new LeaderBoard();
	});

	test('should initialize with empty entries', () => {
		expect(leaderBoard['entries'].length).toBe(0);
	});

	test('should add a new entry correctly', () => {
		leaderBoard.addEntry('Player X', 1, 1);
		expect(leaderBoard['entries'].length).toBe(1);
		expect(leaderBoard['entries'][0]).toEqual({
			name: 'Player X',
			gamesPlayed: 1,
			wins: 1,
		});
	});

	test('should update an existing entry correctly', () => {
		leaderBoard.addEntry('Player X', 1, 1);
		leaderBoard.addEntry('Player X', 1, 0);
		expect(leaderBoard['entries'].length).toBe(1);
		expect(leaderBoard['entries'][0]).toEqual({
			name: 'Player X',
			gamesPlayed: 2,
			wins: 1,
		});
	});

	test('should render the leader board correctly', () => {
		leaderBoard.addEntry('Player X', 1, 1);
		leaderBoard.addEntry('Player O', 1, 0);
		const rows = document.querySelectorAll('#leader-board table tbody tr');
		expect(rows.length).toBe(2);
	});
});
