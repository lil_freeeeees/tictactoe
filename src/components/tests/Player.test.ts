import { Player } from '../Player';

class TestPlayer extends Player {
    makeMove(row: number, col: number): void {
        console.log(`Move made at row ${row}, col ${col}`);
    }
}

describe('Player', () => {
    let player: TestPlayer;

    beforeEach(() => {
        player = new TestPlayer('Test Player', 'X');
    });

    test('should create a player with the correct name and symbol', () => {
        expect(player.name).toBe('Test Player');
        expect(player.symbol).toBe('X');
    });

    test('should call makeMove method correctly', () => {
        const consoleSpy = jest.spyOn(console, 'log');
        player.makeMove(1, 2);
        expect(consoleSpy).toHaveBeenCalledWith('Move made at row 1, col 2');
    });
});
