import { Player } from './Player';

/**
 * HumanPlayer class represents a human-controlled player in the Tic Tac Toe game.
 * This class extends the Player base class and implements interaction with the web interface.
 */
class HumanPlayer extends Player {
	/**
	 * Constructor for creating a new human player.
	 * @param name The name of the player.
	 * @param symbol The game symbol ('X' or 'O') used by the player.
	 */
	constructor(name: string, symbol: 'X' | 'O') {
		super(name, symbol);
		this.setupListeners();
	}

	/**
	 * Sets up event listeners on the game board cells.
	 * This method attaches click events to each cell to enable player moves.
	 */
	private setupListeners(): void {
		const cells = document.querySelectorAll('.cell');
		cells.forEach((cell) => {
			cell.addEventListener('click', (event) => this.handleMove(event));
		});
	}

	/**
	 * Handles a move made by the player by clicking a cell on the board.
	 * @param cell The HTML element of the clicked cell.
	 * @param event The MouseEvent of the click.
	 */
	private handleMove(event: Event): void {
		const cell = event.target as HTMLElement;
		const rowIndex = parseInt(cell.getAttribute('data-row')!);
		const colIndex = parseInt(cell.getAttribute('data-col')!);
		this.makeMove(rowIndex, colIndex);
	}

	/**
	 * Implements the actual logic to make a move on the game board.
	 * Assumes a global state or passed reference to the game board.
	 * @param row The row index where the move is made.
	 * @param col The column index where the move is made.
	 */
	public makeMove(row: number, col: number): void {
		console.log(`Move made at row ${row}, col ${col} by ${this.name}`);

		const cellSelector = `.cell[data-row="${row}"][data-col="${col}"]`;
		const cell = document.querySelector(cellSelector);
		if (cell && cell.textContent === '') {
			cell.textContent = this.symbol;
		}
	}
}

export { HumanPlayer };
