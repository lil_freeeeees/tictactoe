/**
 * Enum for representing the state of the Tic Tac Toe game.
 */
enum GameState {
	CONTINUE = 'CONTINUE',
	X_WINS = 'X_WINS',
	O_WINS = 'O_WINS',
	DRAW = 'DRAW',
	STOPPED = 'STOPPED',
}

export { GameState };
