import { HumanPlayer } from './components/HumanPlayer';
import { GameState } from './enums/GameState';
import { initializeBoard, checkWin, checkDraw } from './utils/gameUtils';
import { ICell } from './interfaces/GameInterface';
import { LeaderBoard } from './components/LeaderBoard';

class TicTacToe {
	private board: ICell[][];
	private currentPlayer: HumanPlayer;
	private playerX: HumanPlayer;
	private playerO: HumanPlayer;
	private gameState: GameState;
	private leaderBoard: LeaderBoard;

	constructor() {
		this.board = initializeBoard();
		this.playerX = new HumanPlayer('Player X', 'X');
		this.playerO = new HumanPlayer('Player O', 'O');
		this.currentPlayer = this.playerX;
		this.gameState = GameState.CONTINUE;
		this.leaderBoard = new LeaderBoard();
		this.setupListeners();
		this.render();
		this.updateStatus(`${this.currentPlayer.name}'s turn`);
	}

	private setupListeners(): void {
		const cells = document.querySelectorAll('.cell');
		cells.forEach((cell) => {
			cell.addEventListener('click', (event) => this.handleCellClick(event));
		});
		const resetButton = document.getElementById('reset-button');
		resetButton?.addEventListener('click', () => this.resetGame());
	}

	private handleCellClick(event: Event): void {
		const cell = event.target as HTMLElement;
		const row = parseInt(cell.dataset.row!);
		const col = parseInt(cell.dataset.col!);

		if (this.board[row][col].value || this.gameState !== GameState.CONTINUE) {
			return;
		}

		this.board[row][col].value = this.currentPlayer.symbol;
		cell.textContent = this.currentPlayer.symbol;

		if (checkWin(this.board)) {
			this.gameState =
				this.currentPlayer.symbol === 'X' ? GameState.X_WINS : GameState.O_WINS;
			this.updateStatus(`${this.currentPlayer.name} wins!`);
			this.leaderBoard.addEntry(this.currentPlayer.name, 1, 1);
		} else if (checkDraw(this.board)) {
			this.gameState = GameState.DRAW;
			this.updateStatus("It's a draw!");
		} else {
			this.switchPlayer();
		}
	}

	private switchPlayer(): void {
		this.currentPlayer =
			this.currentPlayer === this.playerX ? this.playerO : this.playerX;
		this.updateStatus(`${this.currentPlayer.name}'s turn`);
	}

	private updateStatus(message: string): void {
		const status = document.getElementById('status');
		if (status) {
			status.textContent = message;
		}
	}

	private resetGame(): void {
		this.board = initializeBoard();
		this.gameState = GameState.CONTINUE;
		this.currentPlayer = this.playerX;
		this.render();
		this.updateStatus("Player X's turn");
	}

	private render(): void {
		this.board.forEach((row, rowIndex) => {
			row.forEach((cell, colIndex) => {
				const cellElement = document.querySelector(
					`.cell[data-row="${rowIndex}"][data-col="${colIndex}"]`
				);
				if (cellElement) {
					cellElement.textContent = cell.value;
				}
			});
		});
	}
}

new TicTacToe();

function main() {
	document.addEventListener('DOMContentLoaded', () => {
		new TicTacToe();
	});
}

main();
export default TicTacToe;
